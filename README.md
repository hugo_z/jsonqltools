![](public/jsonql-logo.png)

# jsonql tools

This is a collection of JSONql tools starting with jsonql-client and jsonql-koa

## What is jsonql

This is not a replacement of REST or GraphQL, instead we are focus on the `last one mile`.

The communication protocol is based on [JSON API](https://jsonapi.org/).

It's intend for communication between different devices or services.

And the main goal is to write the resolver (on server side) once, and the
client doesn't need any code written and it should work.

It's like this

```
  client <--> contract <--> server
```

## Query, Mutation, Auth

There are only three types of calls. `query`, `mutation` and `auth`

For more information please check one of our following available modules

- [jsonql-koa](https://www.npmjs.com/package/jsonql-koa) json:ql Koa middleware [![NPM](https://nodei.co/npm/jsonql-koa.png?compact=true)](https://npmjs.org/package/jsonql-koa)
- [jsonql-client](https://www.npmjs.com/package/jsonql-client) json:ql Javascript client using Superagent [![NPM](https://nodei.co/npm/jsonql-client.png?compact=true)](https://npmjs.org/package/jsonql-client)
- [jsonql-node-client](https://www.npmjs.com/package/jsonql-node-client) json:ql Node.js client [![NPM](https://nodei.co/npm/jsonql-node-client.png?compact=true)](https://npmjs.org/package/jsonql-node-client)
- [jsonql-contract](https://www.npmjs.com/package/jsonql-contract) json:ql command line util that creates the contract.json [![NPM](https://nodei.co/npm/jsonql-contract.png?compact=true)](https://npmjs.org/package/jsonql-contract)
- [jsonql-constants](https://www.npmjs.com/package/jsonql-constants) json:ql constants for development purpose [![NPM](https://nodei.co/npm/jsonql-constants.png?compact=true)](https://npmjs.org/package/jsonql-constants)

There are several more in the planning stage

- json:ql ws-server the websocket version of jsonql implementation
- ~~json:ql ws-client the websocket client for Browser~~
- ~~json:ql Fetch client, using Fetch (only support modern browser or Mobile web browsers)~~
- json:ql rx-client - this is the new client that replace the fetch / ws client using stream
- json:ql ts-koa - this is the Typescript port for the server side
- json:ql ts-rx-client - the rx client using Typescript
- json:ql Go
- json:ql Dart
- json:ql ASP.NET

If you are interested in creating your implementation please do not hesitate to contact us.

## Work on this project

Please first fork this repo. Then setup your git as follow:

```sh
$ git remote add upstream git@gitlab.com:to1source-open-source/jsonqltools.git
$ git remote -v
$ git fetch upstream
```

Once you are done coding, please PR your code for checking. Please make sure you have
written test accordingly before PR.

## Documentation (coming soon)

Full documentation at [json:ql](https://to1source-open-source.gitlab.io/jsonqltools)

---

MIT

Co-Develop by [to1source LTD China](https://to1source.com) and [NEWBRAN LTD UK](https://newbran.ch)
