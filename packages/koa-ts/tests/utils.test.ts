// testing all the util functions
import test from 'ava';
import {
  dasherize,
  getDocLen,
  headerParser,
  isHeaderPresent, // @TODO
  isJsonqlRequest, // @TODO
  getCallMethod, // @TODO
  getPathToFn, // @TODO
  packResult, // @TODO
  replaceErrors,
  printError,
  packError // @TODO
} from '../src/lib/utils';

test('Testing dasherize', async t => {
  t.is(await dasherize('fileToDash'), 'file-to-dash');
});

test('Testing getDocLen', async t => {
  t.truthy(await getDocLen('<b>Just a test</b>'));
});
