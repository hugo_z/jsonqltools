// jsonql-auth middleware
const debug = require('debug')('jsonql-koa:auth');
const {
  AUTH_TYPE,
  ISSUER_NAME,
  VALIDATOR_NAME,
  AUTH_CHECK_HEADER,
  BEARER
} = require('jsonql-constants');
const {
  getFunction,
  packResult,
  headerParser,
  getDocLen,
  printError
} = require('./lib');
const trim = require('lodash.trim');
// declare a global variable to store the userdata with null value
// this way we don't mess up with the resolver have to check
// the last param is the user data
GLOBAL.CURRENT_LOGIN_USER = null;

/**
 * @param {string} headers from ctx
 * @param {string} type to look for
 * @return {mixed} the bearer token on success
 */
const authHeaderParser = headers => {
  const header = headers[AUTH_CHECK_HEADER];
  // debug('Did we get the token?', header);
  return header ? getToken(header) : false;
}

/**
 * reusable error handler
 * @param {object} ctx koa
 * @param {object} e error
 * @return {undefined} nothing
 */
const forbidden = (ctx, e) => {
  ctx.throw(403, e.message, {error: printError(e)});
}

/**
 * just return the token string
 * @param {string} header
 * @return {string} token
 */
const getToken = header => {
  return trim(header.replace(BEARER, ''));
}

/**
 * Just check what is this calling and don't want to throw at this point
 * @param {string} method how they calling this if (ctx.method === 'POST') {
 * @param {mixed} payload expecting object
 * @param {object} ctx koa
 * @param {object} opt config
 * @return {boolean} true yes
 */
const checkIsLogin = function(payload, ctx, opts) {
  if (ctx.method !== 'POST') {
    return false;
  }
  // debug('got this payload to check', payload);
  try {
    const headers = headerParser(ctx.request, opts.contentType);
    // debug('headers again?', headers);
    if (headers.length) {
      // this is actually not great because the word issuer / validator become reserved words
      return payload[ISSUER_NAME];
    }
    debug('fail check from payload', printError(payload));
    return false;
  } catch(e) {
    debug('checkIsLogin error?', printError(e));
    return false;
  }
}

/**
 * Auth middleware, we support
 * 1) OAuth 2
 * 2) JWT token
 * This is just front we don't do any real auth here,
 * instead we expect you to supply a function and pass you data and let you
 * handle the actual authentication
 */
module.exports = function(config) {
  // get the auth config what should be in there?
  const authConfig = config.auth;
  const jsonqlPath = config.jsonqlPath;
  // const issuerPath = authConfig.issuerPath || jsonqlPath;
  // return middleware
  return async function(ctx, next) {
    // we should only care if there is api call involved
    if (ctx.request.isJsonqlRequest) {
      let result;
      let issuerFnPath;
      const contract = ctx.request.jsonqlContract;
      const payload = ctx.request.jsonqlPayload;
      const isPayload = typeof payload === 'object';
      const isLogin = isPayload ? checkIsLogin(payload, ctx, config) : false;
      debug('Is this calling the login?', isLogin);
      // @TODO should there be a different url for the auth which is open (disable for now)
      if (isLogin) {
        try {
          issuerFnPath = getFunction(ISSUER_NAME, AUTH_TYPE, config, contract);
        } catch (e) {
          ctx.throw(404, e.message || 'Auth method not found!');
        }
        // next try to validate login
        try {
          const issuerFn = require(issuerFnPath);
          result = await issuerFn.apply(null, payload[ISSUER_NAME].args);
          if (!result) {
            debug('issuer failed', result);
            throw new Error('Issuer failed');
          }
          const body = packResult(result);
          ctx.size = getDocLen(body);
          ctx.type = config.contentType;
          ctx.status = 200;
          ctx.body = body;
          return;
        } catch(e) {
          // throw forbidden
          debug('throw at login failed', e);
          ctx.throw(401, e.message, {error: e});
        }
      } else if (isPayload && !isLogin) {
        let validatorFnPath;
        try {
          validatorFnPath = getFunction(VALIDATOR_NAME, AUTH_TYPE, config, contract);
        } catch(e) {
          ctx.throw(404, e.message || 'Validator method not found!');
        }
        try {
          const validatorFn = require(validatorFnPath);
          const token = authHeaderParser(ctx.request.headers);
          if (token) {
            result = await validatorFn(token);
            if (result !== false && result !== undefined && result !== null && result !== '') {
              // here we add the userData to the global
              ctx.request.userData = GLOBAL.CURRENT_LOGIN_USER = result;
              await next();
            } else {
              debug('throw at wrong result', result);
              forbidden(ctx, {message: 'result is empty?'});
            }
          } else {
            debug('throw at headers not found', ctx.request.headers);
            forbidden(ctx, {message: 'header is not found!'});
          }
        } catch(e) {
          debug('throw at some where throw error', e);
          forbidden(ctx, e);
        }
      }
    } else {
      await next();
    }
  }
}
