// This is the first part of the middlewares that try to work out the contract
// the reason is, it will get re-use by subsequences middlewares
// so we might as well do this here
// Also we could hijack it off and serve the html files up for documentation purpose
const {
  getDocLen,
  isJsonqlRequest,
  contractFn,
  readContract
} = require('./lib');
const fs = require('fs');
const fsx = require('fs-extra');
const { join } = require('path');
const debug = require('debug')('jsonql-koa:contract');
const trim = require('lodash.trim');

/**
 * get the contract data @TODO might require some sort of security here
 * @param {object} opts options
 * @param {object} ctx koa
 * @return {undefined}
 */
const handleContract = (opts, ctx, contract) => {
  debug('serving up contract', contract);
  const body = JSON.stringify(contract);
  ctx.size = getDocLen(body);
  ctx.type = opts.contentType;
  ctx.status = 200;
  ctx.body = body;
};

/**
 * Handle contract authorisation using a key
 * @param {object} ctx koa
 * @param {object} opts options
 * @return {boolean} true ok
 */
const contractAuth = function(ctx, opts) {
  if (opts.contractKey !== false && opts.contractKeyName !== false) {
    const { contractKey, contractKeyName } = opts;
    debug('Received this for auth', contractKeyName, contractKey);
    const keyValueFromClient = trim(ctx.query[contractKeyName]);
    debug('the query value', ctx.query);
    switch (true) {
      case typeof contractKey === 'string':
          debug('compare this two', keyValueFromClient, contractKey);
          return keyValueFromClient === contractKey;
        break;
      case typeof contractKey === 'function':
          return contractKey(keyValueFromClient);
        break;
      default:
        // @TODO what if we want to read the header?
        debug('Unsupported contract auth type method', typeof contractKey);
        return false;
    }
  }
  return true;
};

// export
module.exports = function(opts) {
  // export
  return async function(ctx, next) {
    ctx.request.isJsonqlRequest = isJsonqlRequest(ctx, opts);
    if (ctx.method === 'HEAD' || ctx.method === 'GET') {
      if (ctx.request.isJsonqlRequest) {
        if (contractAuth(ctx, opts)) {
          // we stop here
          const contractJson = await contractFn(opts, true);
          debug('call handle public contract method here');
          // this is a public contract
          return handleContract(opts, ctx, contractJson);
        } else {
          ctx.throw(401, 'Unauthorized access to contract');
        }
      } else {
        // @TODO plugin a render html from contract middleware
      }
    }
    debug('pass here and should generate internal contract');
    let contract = readContract(opts.contractDir);
    if (contract === false) {
      contract = await contractFn(opts);
    }
    // this is a private contract
    ctx.request.jsonqlContract = contract;
    await next();
  }
}
