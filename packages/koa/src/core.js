// The core of the jsonql middleware
const fs = require('fs');
const { join } = require('path');
const debug = require('debug')('jsonql-koa:core');
const {
  headerParser,
  getFunction,
  getDocLen,
  packResult,
  packError,
  printError,
  ResolverNotFoundError,
  ResolverApplicationError
} = require('./lib');
/**
 * Top level entry point for jsonql Koa middleware
 * @param {object} config options
 * @return {mixed} depends whether if we catch the call
 */
module.exports = function(opts) {
  /**
   * The method call has this signature
   * @param {object} ctx Koa
   * @param {string} type of calls
   * @param {object} contract to search via the file name info
   * @return {mixed} depends on the contract
   */
  const resolveMethod = async (ctx, type, contract = {}) => {
    const payload = ctx.request.body;
    debug('middleware payload', payload);
    if (typeof payload === 'object') {
      let fn;
      let pathToFn;
      let result;
      let args = [];
      // There must be only one method call
      const name = Object.keys(payload)[0];
      // first try to catch the resolve error
      try {
        pathToFn = getFunction(name, type, opts, contract);
      } catch (e) {
        if (e instanceof ResolverNotFoundError) {
          ctx.throw(404, 'Resolver Not Found!');
        } else {
          ctx.throw(500, 'Server failure! [42]');
        }
        debug('Resolve function error', printError(e));
        return;
      }
      // we now include it here
      try {
        fn = require(pathToFn);
      } catch(e) {
        debug('able to conceal the error here');
        const body = packError(e);
        ctx.size = getDocLen(body);
        ctx.type = opts.contentType;
        ctx.status = 200;
        ctx.body = body;
        return;
      }
      // catch application level error
      try {
        if (type === 'query') {
          args = payload[name].args || []; // if there is nothing then just empty array
          // make sure it's an array for the apply method
          if (!Array.isArray(args)) {
            args = [args];
          }
        } else {
          // have to make sure there is some value to pass to it
          const _payload = payload[name].payload || null;
          const _conditions = payload[name].conditions || null;
          args = [_payload, _conditions];
        }
        // we push the userData to the last of the argument
        if (opts.auth && ctx.request.userData) {
          args.push(ctx.request.userData);
        }
        result = await fn.apply(null, args);
        const body = packResult(result);

        debug('called and now serve up', body);

        ctx.size = getDocLen(body);
        ctx.type = opts.contentType;
        ctx.status = 200;
        ctx.body = body;
        return;
      } catch (e) {

        debug('executing resolver error', printError(e));

        const body = packError(e);
        ctx.size = getDocLen(body);
        ctx.type = opts.contentType;
        ctx.status = 200;
        ctx.body = body;
        return;
      }
    } else {
      ctx.throw(406, 'Payload is not the expected object type', payload);
    }
  };

  /**
   * @TODO need to be more flexible
   * @param {object} ctx koa
   * @param {object} opts configuration
   * @return {boolean} if it match
   */
  const resolvingPath = (ctx, opts) => {
    // debug('ctx.path', ctx.path, 'ctx.query', ctx.query);
    return ctx.path === opts.jsonqlPath;
  };

  // ouput the middleware
  return async function(ctx, next) {
    const contractJson = ctx.request.jsonqlContract || false;
    // debug('do we have a contract here?', contractJson); <-- yes we do
    if (resolvingPath(ctx, opts)) {
      const headers = headerParser(ctx.request, opts.contentType);
      if (headers.length) {
        if (ctx.method === 'POST') {
          // Now we will grab the input and figure out what to do with it
          return resolveMethod(ctx, 'query', contractJson);
        } else if (ctx.method === 'PUT') {
          return resolveMethod(ctx, 'mutation', contractJson);
        }
      }
    }
    await next();
  };
};
