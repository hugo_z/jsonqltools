// main export interface
const defaultOptions = require('./lib/options');
const jsonqlAuthMiddleware = require('./auth');
const jsonqlCoreMiddleware = require('./core');
const jsonqlContractMiddleware = require('./contract');
const helloMiddleware = require('./hello');
const clientGenerator = require('./client');
// export
module.exports = {
  defaultOptions,
  jsonqlAuthMiddleware,
  jsonqlCoreMiddleware,
  jsonqlContractMiddleware,
  helloMiddleware,
  clientGenerator
};
