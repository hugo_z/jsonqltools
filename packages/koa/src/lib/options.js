const { join } = require('path');
const {
  JSONQL_PATH,
  CONTENT_TYPE,
  DEFAULT_RESOLVER_DIR,
  DEFAULT_CONTRACT_DIR,
  CONTRACT_KEY_NAME
} = require('jsonql-constants');
// @BUG when we deploy it in docker, or using systemd the workingDirectory affect the
// execute path, it might be better to allow a config option of workingDirectory and
// use that as base
const dirname = process.cwd();
// @TODO we need to create the same fn to clear out the options like I did in server-io-core
module.exports = {
  auth: false,
  bodyParser: {},
  jsonqlPath: '/' + JSONQL_PATH,
  contentType: CONTENT_TYPE,
  resolverDir: join(dirname, DEFAULT_RESOLVER_DIR),
  contractDir: join(dirname, DEFAULT_CONTRACT_DIR),
  contractKey: false, // String || Function
  contractKeyName: CONTRACT_KEY_NAME,  // String
  autoCreateContract: true,
  // Perhaps I should build the same create options style like server-io-core
  buildContractOnStart: process.env.NODE_ENV === 'development',
  // new feature for v.1.1 release
  // if the developer pass the nodeClient config then we will pre-generate the calling method
  // for them. We expect them to named the client so it will be key:value pair
  nodeClient: false, // this will require a config item
  exposeError: null // this will allow you to control if you want to throw your error back to your client
};
