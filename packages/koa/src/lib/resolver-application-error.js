'use strict';
/**
 * We will export this for the resolver to use if they happen to encounter error
 * in their code
 * @param {string} message to tell what happen
 * @param {mixed} extra things we want to addd, such as 404
 */
module.exports = function ResolverApplicationError(message, extra) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message = message;
  this.extra = extra;
};

require('util').inherits(module.exports, Error);
