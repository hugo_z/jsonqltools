// this need to get call from the beginning
// so make this available in different places
const fsx = require('fs-extra');
const { join } = require('path');
const contractApi = require('jsonql-contract');
const {
  DEFAULT_FILE_NAME,
  PUBLIC_FILE_NAME
} = require('jsonql-constants');

/**
 * @param {string} contractDir where the contract is
 * @param {boolean} pub system of public
 * @return {mixed} false on not found
 */
const readContract = function(contractDir, pub) {
  const file = join(contractDir, pub ?  PUBLIC_FILE_NAME : DEFAULT_FILE_NAME);
  if (fsx.existsSync(file)) {
    return fsx.readJsonSync(file);
  }
  return false;
};

/**
 * contract create handler
 * @param {object} opts options
 * @param {boolean} pub where to serve this
 * @return {object} promise to resolve contract json
 */
const contractFn = function(opts, pub = false) {
  return new Promise((resolver, rejecter) => {
    if (opts.buildContractOnStart === false) {
      const contract = readContract(opts.contractDir, pub);
      if (contract !== false) {
        return resolver(contract);
      }
    }
    contractApi({
      public: pub,
      outDir: opts.contractDir,
      inDir: opts.resolverDir,
      useDoc: opts.useDoc,
      auth: opts.auth
    }).then(newFile => {
      return resolver(fsx.readJsonSync(newFile));
    }).catch(rejecter);
  });
};
// export
module.exports = {
  contractFn,
  readContract
};
