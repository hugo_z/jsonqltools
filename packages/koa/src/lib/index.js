// lib main export
const {
  headerParser,
  getDocLen,
  packError,
  packResult,
  printError,
  isJsonqlRequest,
  getCallMethod,
  isHeaderPresent
} = require('./utils');
const getFunction = require('./search');
const { contractFn, readContract } = require('./contract');
const ResolverNotFoundError = require('./resolver-not-found-error');
const ResolverApplicationError = require('./resolver-application-error');
// export
module.exports = {
  headerParser,
  getDocLen,
  packError,
  packResult,
  printError,
  // individual import
  getFunction,
  isJsonqlRequest,
  getCallMethod,
  isHeaderPresent,
  contractFn,
  readContract,
  ResolverNotFoundError,
  ResolverApplicationError
};
