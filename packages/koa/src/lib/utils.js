const trim = require('lodash.trim');
const { join } = require('path');
const fs = require('fs');
const { inspect } = require('util');
/**
 * From underscore.string library
 * @param {string} str string
 * @return {string} dasherize string
 */
const dasherize = str => {
  return trim(str)
    .replace(/([A-Z])/g, '-$1')
    .replace(/[-_\s]+/g, '-')
    .toLowerCase();
};

/**
 * Get document (string) byte length for use in header
 * @param {string} doc to calculate
 * @return {number} length
 */
const getDocLen = doc => {
  return Buffer.byteLength(doc, 'utf8');
};

/**
 * The koa ctx object is not returning what it said on the documentation
 * So I need to write a custom parser to check the request content-type
 * @param {object} req the ctx.request
 * @param {string} type (optional) to check against
 * @return {mixed} Array or Boolean
 */
const headerParser = (req, type) => {
  try {
    const headers = req.headers.accept.split(',');
    if (type) {
      return headers.filter(h => h === type);
    }
    return headers;
  } catch (e) {
    // When Chrome dev tool activate the headers become empty
    return [];
  }
};

/**
 * wrapper of above method to make it easier to use
 * @param {object} req ctx.request
 * @param {string} type of header
 * @return {boolean}
 */
const isHeaderPresent = (req, type) => {
  const headers = headerParser(req, type);
  return !!headers.length;
};

/**
 * combine two check in one and save time
 * @param {object} ctx koa
 * @param {object} opts config
 * @return {boolean} check result
 */
const isJsonqlRequest = (ctx, opts) => {
  const header = isHeaderPresent(ctx.request, opts.contentType);
  if (header) {
    return ctx.path === opts.jsonqlPath;
  }
  return false;
}

/**
 * getting what is calling after the above check
 * @param {string} method of call
 * @return {mixed} false on failed
 */
const getCallMethod = method => {
  switch (method) {
    case 'POST':
      return 'query';
    case 'PUT':
      return 'mutation';
    default:
      return false;
  }
}


/**
 * @param {string} name
 * @param {string} type
 * @param {object} opts
 * @return {function}
 */
const getPathToFn = function(name, type, opts) {
  const dir = opts.resolverDir;
  const fileName = dasherize(name);
  let paths = [];
  if (opts.contract && opts.contract[type] && opts.contract[type].path) {
    paths.push(opts.contract[type].path);
  }
  paths.push( join(dir, type, fileName, 'index.js') );
  paths.push( join(dir, type, fileName + '.js') );
  // paths.push( join(dir, fileName + '.js') );
  const ctn = paths.length;
  for (let i=0; i<ctn; ++i) {
    if (fs.existsSync(paths[i])) {
      return paths[i];
    }
  }
  return false;
}

/**
 * wrapper method
 * @param {mixed} result of fn return
 * @return {string} stringify data
 */
const packResult = result => {
  return JSON.stringify({ data: result });
}

/**
 * Port this from the CIS App
 * @param {string} key of object
 * @param {mixed} value of object
 * @return {string} of things we after
 */
function replaceErrors(key, value) {
  if (value instanceof Error) {
    var error = {};
    Object.getOwnPropertyNames(value).forEach(function (key) {
      error[key] = value[key];
    });
  return error;
  }
  return value;
}

/**
 * create readible string version of the error object
 * @param {object} error obj
 * @return {string} printable result
 */
function printError(error) {
  //return 'MASKED'; //error.toString();
  // return JSON.stringify(error, replaceErrors);
  return inspect(error, false, null, true);
}

/**
 * wrapper method
 * @param {mixed} e of fn error
 * @return {string} stringify error
 */
function packError(e) {
  return JSON.stringify({ error: printError(e) });
}

module.exports = {
  dasherize,
  headerParser,
  getPathToFn,
  getDocLen,
  packResult,
  packError,
  printError,
  isJsonqlRequest,
  getCallMethod,
  isHeaderPresent
};
