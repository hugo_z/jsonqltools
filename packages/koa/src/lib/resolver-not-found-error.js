'use strict';
/**
 * This is a custom error to throw when we couldn't find the resolver
 * This help us to capture the right error, due to the call happens in sequence
 * @param {string} message to tell what happen
 * @param {mixed} extra things we want to addd, such as 404
 */
module.exports = function ResolverNotFoundError(message, extra = 404) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message = message;
  this.extra = extra
};

require('util').inherits(module.exports, Error);
