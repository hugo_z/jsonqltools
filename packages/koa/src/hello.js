// The helloWorld should always be available. Because this can serve as a ping even when its auth:true
const {
  headerParser,
  getDocLen,
  packResult,
  printError,
  isJsonqlRequest,
  getCallMethod
} = require('./lib');

// export
module.exports = function(opts) {
  return async function(ctx, next) {
    // so here we check two things, the header and the url if they match or not
    if (ctx.request.isJsonqlRequest) {
      const method = getCallMethod(ctx.method);
      const payload = ctx.request.body;
      ctx.request.jsonqlMethod = method;
      ctx.request.jsonqlPayload = payload;
      // and finally
      if (method === 'query') {
        if (typeof payload === 'object') {
          const name = Object.keys(payload)[0];
          // add a stock helloWorld method
          if (name === 'helloWorld') {
            // debug('********* ITS CALLING THE HELLO WORDLD *********');
            const body = packResult('Hello world!');
            ctx.size = getDocLen(body);
            ctx.type = opts.contentType;
            ctx.status = 200;
            ctx.body = body;
            return;
          }
        }
      }
    }
    await next();
  }
}
