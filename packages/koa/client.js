const { join } = require('path');
const fs = require('fs');
const fsx = require('fs-extra');
const merge = require('lodash.merge');
const debug = require('debug')('jsonql-koa:client');
const jsonqlNodeClient = require('jsonql-node-client');
// This is for the resolvers to include and get their node-client
module.exports = function(name, config) {
  if (!name) {
    throw new Error('Name is required!');
  }
  if (!config.contractDir) {
    throw new Error('contractDir is required');
  }
  config.contractDir = join(config.contractDir, name);
  if (config) {
    return jsonqlNodeClient(config);
  }
  throw new Error(`${name} client not found!`);
}
