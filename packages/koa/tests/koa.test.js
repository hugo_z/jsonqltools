const test = require('ava');
const Koa = require('koa');
const superkoa = require('superkoa');
const { join } = require('path');
const debug = require('debug')('jsonql-koa:test:koa');
const bodyparser = require('koa-bodyparser');
const jsonqlMiddleware = require(join(__dirname, '..', 'index'));
const { type, headers, dirs } = require('./fixtures/options');
const fsx = require('fs-extra');

const { resolverDir, contractDir } = dirs;

test.before((t) => {
  const app = new Koa();
  app.use(bodyparser());
  app.use(jsonqlMiddleware({
    resolverDir,
    contractDir,
    useDoc: true
  }));
  t.context.app = app;
});

test.after( () => {
  // remove the files after
  fsx.removeSync(join(contractDir, 'contract.json'));
});

// start test

test("Hello world test", async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .set(headers)
    .send({
      helloWorld: {
        args: []
      }
    });
  t.is(200, res.status);
  t.is('Hello world!', res.body.data);
});

test("It should return a hello world contract", async (t) => {
  let res = await superkoa(t.context.app)
    .get('/jsonql')
    .set(headers);
  t.is(200, res.status);
  t.is('string', res.body.query.helloWorld.returns);
});

// start test
test('It should return json object',async (t) => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .set(headers)
    .send({
      testList: {
        args: [1]
      }
    });

  debug('post test output body', res.body);

  t.is(200, res.status);
  t.is(1, res.body.data.num);
});

test('It should change the json object', async(t) => {
  let res = await superkoa(t.context.app)
    .put('/jsonql')
    .set(headers)
    .send({
      updateList: {
        payload: {
          user: 1
        },
        conditions: {
          where: 'nothing'
        }
      }
    });
  t.is(200, res.status);
  t.is(2, res.body.data.user);
});
