// we test all the fail scenario here
const test = require('ava');
const Koa = require('koa');
const superkoa = require('superkoa');
const { join } = require('path');
const debug = require('debug')('jsonql-koa:test:fail');
const bodyparser = require('koa-bodyparser');
const jsonqlMiddleware = require(join(__dirname, '..', 'index'));
const { type, headers, dirs } = require('./fixtures/options');
const fsx = require('fs-extra');
const myKey = '4670994sdfkl';

test.before((t) => {
  const app = new Koa();
  app.use(bodyparser());
  app.use(jsonqlMiddleware({
    resolverDir: dirs.resolverDir,
    contractDir: dirs.contractDir,
    contractKey: myKey,
    contractKeyName: 'myKey',
    useDoc: true
  }));
  t.context.app = app;
});

test.after( () => {
  // remove the files after
  fsx.removeSync(join(dirs.contractDir, 'contract.json'));
});


test("It should Not return a contract", async (t) => {
  let res = await superkoa(t.context.app)
    .get('/jsonql')
    .query({_cb: Date.now()})
    .set(headers);

  t.is(401, res.status);
});


test("It should able to get contract with a key", async (t) => {
  let res = await superkoa(t.context.app)
    .get('/jsonql')
    .query({_cb: Date.now(), myKey})
    .set(headers);

  t.is(200, res.status);
  t.is('string', res.body.query.helloWorld.returns);
});
