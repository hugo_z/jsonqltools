// reusable options
const { join } = require('path');
const type = 'application/vnd.api+json';
const headers = {
  'Accept': type,
  'Content-Type': type
};

const dirs = {
  resolverDir: join(__dirname, 'resolvers'),
  contractDir: join(__dirname, 'contracts')
};

const secret = 'A little cat is watching you';

const bearer = 'sofud89723904lksd98234230823jlkjklsdfds';

module.exports = { type, headers, dirs, secret, bearer };
