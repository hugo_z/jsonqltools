// This is a new query method to test out if we actually get the userData props at the end of call
/**
 * This use a spread as parameter
 * @param {...string} args passing unknown number of param
 * @return {any} extract from last of the args 
 */
module.exports = function(...args) {
  const ctn = args.length;
  const lastProp = args[ctn - 1];
  // if it's login then it should be userData
  return lastProp;
}
