const debug = require('debug')('jsonql-koa:resolver:query:testList');
/**
 * @param {number} num a number
 * @return {object} @TODO need to figure out how to give keys to the returns
 */
module.exports = function(num) {
  debug('Call testList with this params', num);
  return {
    modified: Date.now(),
    text: 'There is something else',
    num: num || -1
  };
};
