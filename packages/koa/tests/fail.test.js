// we test all the fail scenario here
const test = require('ava');
const Koa = require('koa');
const superkoa = require('superkoa');
const { join } = require('path');
const debug = require('debug')('jsonql-koa:test:fail');
const bodyparser = require('koa-bodyparser');
const jsonqlMiddleware = require(join(__dirname, '..', 'index'));
const { type, headers, dirs } = require('./fixtures/options');
const fsx = require('fs-extra');

test.before((t) => {
  const app = new Koa();
  app.use(bodyparser());
  app.use(jsonqlMiddleware({
    resolverDir: dirs.resolverDir,
    contractDir: dirs.contractDir,
    useDoc: true
  }));
  t.context.app = app;
});

test.after( () => {
  // remove the files after
  fsx.removeSync(join(dirs.contractDir, 'contract.json'));
});

test("Should fail this Hello world test", async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql/somethingelse?_cb=9082390483204830')
    .set(headers)
    .send({
      helloWorld: {
        args: {}
      }
    });
  t.is(404, res.status);
  // t.is('Hello world!', res.body.data);
});
