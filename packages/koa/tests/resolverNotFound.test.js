// this one will test the resolver not found and the application error

const test = require('ava');
const server = require('./helpers/server');
const superkoa = require('superkoa');
const { headers } = require('./fixtures/options');
const debug = require('debug')('jsonql-koa:test:ResolverNotFoundError');
const ResolverNotFoundError = require('../src/lib/resolver-not-found-error');

test.before( t => {
  t.context.app = server();
});

test('it should throw a ResolverNotFoundError', async (t) => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .set(headers)
    .send({
      helloWorld2: {
        args: []
      }
    });
  t.is(true, res.status === 404);
});

test("It should cause an Application error but nothing throw", async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .set(headers)
    .send({
      causeError: {
        args: [1]
      }
    });
  t.is(true, res.status ===  200);
});
