const test = require('ava');
const Koa = require('koa');
const superkoa = require('superkoa');
const { join } = require('path');
const debug = require('debug')('jsonql-koa:test:auth');
const bodyparser = require('koa-bodyparser');
const jsonqlMiddleware = require(join(__dirname, '..', 'index'));
const { type, headers, dirs, bearer } = require('./fixtures/options');
const fsx = require('fs-extra');
const myKey = '4670994sdfkl';
const merge = require('lodash.merge');

test.before((t) => {
  const app = new Koa();
  app.use(bodyparser());
  app.use(jsonqlMiddleware({
    resolverDir: dirs.resolverDir,
    contractDir: dirs.contractDir,
    contractKey: myKey,
    contractKeyName: 'myKey',
    auth: true,
    useDoc: true
  }));
  t.context.app = app;
});

test.after( () => {
  // remove the files after
  fsx.removeSync(join(dirs.contractDir, 'contract.json'));
});

// Start running test(s)

test("Should NOT fail this Hello world test even I am not login", async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .query({_cb: Date.now()})
    .set(headers)
    .send({
      helloWorld: {
        args: []
      }
    });
  t.is(200, res.status);
});

test('Should able to login with this credential', async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .query({_cb: Date.now()})
    .set(headers)
    .send({
      issuer: {
        args: ['nobody', myKey]
      }
    });
  t.is(200, res.status);
  t.is(bearer, res.body.data);
});

test('Now I should able to call the api with credential', async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .query({_cb: Date.now()})
    .set(merge({} , headers, {Authorization: `Bearer ${bearer}`}))
    .send({
      getUser: {
        args: ['testing']
      }
    });

  t.is(200, res.status);
  t.is(1, res.body.data.userId);
});

test.todo('Should able to retrieve a userdata object from the GLOBAL.CURRENT_LOGIN_USER');
