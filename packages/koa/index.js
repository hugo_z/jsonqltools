'use strict';
/**
 * This is the main interface to export the middleware(s)
 * we will take the config here and export array of middleware using koa-compose
 */
const fs = require('fs');
const merge = require('lodash.merge');
const compose = require('koa-compose');
const debug = require('debug')('jsonql:koa:main');
const {
  defaultOptions,
  jsonqlCoreMiddleware,
  jsonqlAuthMiddleware,
  jsonqlContractMiddleware,
  helloMiddleware,
  clientGenerator
} = require('./src');
const { contractFn } = require('./src/lib');
// main
module.exports = function(config = {}) {
  // create configuration options
  const opts = merge({}, defaultOptions, config);
  // check the require parameters
  if (!opts.resolverDir) {
    // should just do a 500 here!
    throw new Error('You must provide the resolverDir!');
  }
  if (!fs.existsSync(opts.resolverDir)) {
    throw new Error('The Resolver Directory does not exist!');
  }
  // generate system contract at start up
  contractFn(opts);
  // call the client generator
  /* scrap this due to the problem we need to create a file
  if (config.nodeClient !== false && typeof config.nodeClient === 'object') {
    clientGenerator(config);
  }
  */
  // announcement
  debug('startup jsonql with', opts);
  // export
  const middlewares = [
    jsonqlContractMiddleware(opts),
    helloMiddleware(opts),
    jsonqlCoreMiddleware(opts)
  ];
  if (opts.auth !== undefined && opts.auth !== false) {
    debug('adding auth to the middlewares');
    middlewares.splice(2, 0, jsonqlAuthMiddleware(opts));
  }
  // finally
  return compose(middlewares);
}
