import { generator, fetchContract } from './generator';
import options from './options';
import merge from 'lodash.merge';
import { getContract } from './fetch';

// main
export default function(config = {}) {
  const opts = merge({}, options, config);
  return fetchContract(opts, opts.contract)
    .then(contract => generator(opts, contract));
}
