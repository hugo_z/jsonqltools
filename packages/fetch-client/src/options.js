import {
  JSONQL_PATH,
  CONTENT_TYPE,
  BEARER,
  CLIENT_STORAGE_KEY,
  CLIENT_AUTH_KEY,
  CONTRACT_KEY_NAME
} from 'jsonql-constants';

export default {
  hostname: '', // required the hostname
  jsonqlPath: JSONQL_PATH, // The path on the server
  contentType: CONTENT_TYPE, // the header
  useLocalstorage: true, // should we store the contract into localStorage
  storageKey: CLIENT_STORAGE_KEY, // the key to use when store into localStorage
  authKey: CLIENT_AUTH_KEY, // the key to use when store into the sessionStorage
  contract: false,
  contractKey: false, // if the server side is lock by the key you need this
  contractKeyName: CONTRACT_KEY_NAME, // same as above they go in pairs
  authPrefix: BEARER,
  timeout: 5000, // 5 seconds
  allowReturnRawToken: false,
  fetchBase: {
    mode: 'cors', // no-cors, cors, *same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    redirect: 'follow', // manual, *follow, error
    referrer: 'no-referrer',
    credentials: 'omit'
  }
};
