// The final interface

import { getContract, query, mutation } from './fetch';
import { curry, storeAuthToken, getAuthTokenHeader } from './helpers';
import debug from 'debug';

const AUTH_FN = 'issuer';
/**
 * handle the return data
 * @param {object} result return from server
 * @return {object} strip the data part out, or if the error is presented
 */
const resultHandler = result => {
  if (result.data && !result.error) {
    return result.data;
  }
  throw new Error(result.error);
}
// re-export to use later
const createEvt = name => ['jsonql', name.toLowerCase(), 'abort'].join('-');

// generate a event listener for the abort signal
const createAbortSignal = name => {
  const { controller, signal } = createAbortContrller();
  const evt = createEvt(name);
  document.addEventListener(evt, () => {
    debug(evt, 'aborted');
    controller.abort();
  }, false);
  return { signal, evt};
};

// remove the event listener for the abort signal
const removeAbortSignal = evt => {
  debug(evt, 'removed');
  document.removeEventListener(evt);
};

const createEvt = (name, type = 'query') => ['jsonql', type, name.toLowerCase(), 'abort'].join('-');

const onAbort = (evt, ctrl) => {
  document.addEventListner(evt, () => {
    debug(evt, 'call abort');
    ctrl.abort();
  }, false);
}

const offAbort = evt => {
  debug(evt, 'remove abort signal');
  document.removeEventListner(evt);
}

const fetchContract = (config, contract) => {
  if (contract && contract.query && contract.mutation) {
    return Promise.resolve(contract);
  }
  const evt = createEvt('fetchContract', 'helper');
  const ctrl = new AbortController();
  const signal = ctrl.signal;
  const tmpFn = curry(getContract)(config);
  onAbort(evt);
  return tmpFn.apply(null, [signal])
    .then(result => {
      offAbort(evt);
    return result;
    });
}

/**
 * @param {object} config options
 * @param {object} contract optional static contract
 * @return {object} constructed functions call
 */
const generator = (config, contract) => {
  let obj = {query: {}, mutation: {}, auth: {}};
  // process the query first
  for (let queryFn in contract.query) {
    // the first three params config, name, signal curry into the first part
    obj.query[queryFn] = (...args) => {
      const evt = createEvt(queryFn);
      const ctrl = new AbortController();
      const signal = ctrl.signal;
      const tmpQueryFn = curry(query)(config, queryFn, signal);
      onAbort(evt, ctrl);
      // this is because we wrap all into one array pass to query
      const qParams = contract.query[queryFn].params;
      const params = qParams.map((p, i) => args[i]);
      const header = args[params.length] || {};
      // @TODO validate against the type
      return tmpQueryFn.apply(null, [params, header])
        .then(result => {
          offAbort(evt);
        return result;
        })
        .then(resultHandler);
    };
  }
  // process the mutation, the reason the mutation has a fixed number of parameters
  for (let mutationFn in contract.mutation) {
    obj.mutation[mutationFn] = (...args) => {
      const evt = createEvt(mutationFn, 'mutation');
      const ctrl = new AbortController();
      const signal = ctrl.signal;
      const tmpMutationFn = curry(mutation)(config, mutationFn, signal);
      const params = [
        args[0], // payload
        args[1] || {}, // conditions
        args[2] || {} // header if any
      ];
      onAbort(evt, ctrl);
      return tmpMutationFn.apply(null, params)
        .then(result => {
          offAbort(evt);
        return result;
        })
        .then(resultHandler);
    };
  }
  // there is only one call issuer we want
  if (contract.auth) {
    obj.auth = (...args) => {
      debug('auth', params);
      const params = contract.auth.issuer.params.map((p, i) => args[i]);
      const header = args[params.length] || {};
      const evt = createEvt(AUTH_FN, 'auth');
      const ctrl = new AbortController();
      const signal = ctrl.signal;
      const tmpAuthFn = curry(query)(config, AUTH_FN, signal);
      onAbort(evt, ctrl);

      return tmpAuthFn.apply(null, [params, header])
        .then(result => {
          offAbort(evt);
        return result;
        })
        .then(result => {
          if (result.data && result.data.token) {
            return storeAuthToken(config, result.data.token);
          }
          throw new Error(result.error || 'Auth failed');
        });
    }
  }
  if (config.allowReturnRawToken) {
    obj.getAuthTokenHeader = curry(getAuthTokenHeader)(config);
  }
  // output
  return obj;
};

export {
  generator,
  fetchContract
}
