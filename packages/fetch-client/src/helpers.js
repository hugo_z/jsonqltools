// this part is all about the storage how

export function storeContract(config, contract) {
  if (config.useLocalstorage === true && typeof contract === 'object') {
    if (window && window.localStorage) {
      window.localStorage.setItem(config.storageKey, JSON.stringify(contract));
    }
  }
  return false;
}

export function readContract(config) {
  if (config.contract && typeof config.contract === 'object') {
    return config.contract;
  }
  if (config.useLocalstorage === true) {
    if (window && window.localStorage) {
      const contract = window.localStorage.getItem(config.storageKey);
      return contract ? JSON.parse(contract) : false;
    }
  }
  return false;
}

export function storeAuthToken(config, token) {
  if (window && window.sessionStorage) {
    window.sessionStorage.setItem(config.authKey, token);
    return token;
  }
  throw new Error('No session storage support in your environment!');
}

export function getAuthTokenHeader(config) {
  if (window && window.sessionStorage) {
    const token = window.sessionStorage.getItem(config.authKey);
    return token ? {Authorization: `${config.authPrefix} ${token}`} : {};
  }
  return {};
}

export function baseHeader(config) {
  const { contentType } = config;
  return {
    'Accept': contentType,
    'Content-Type': contentType
  };
}

export function getCb() {
  return '?_cb=' + Date.now();
}

export function getQuery(config) {
  let query = getCb();
  if (config.contractKey !== false) {
    query += '&' + [config.contractKeyName, config.contractKey].join('=');
  }
  return query;
}

export function getUrl(config, getContract = false) {
  let url = [config.hostname, config.jsonqlPath].join('/');
  if (getContract) {
    url += getQuery(config);
  }
  return url;
}

export function createHeaders(config, headers) {
  const authHeader = getAuthTokenHeader(config);
  return Object.assign({}, baseHeader(), authHeader, headers);
}

export const curry = fn => (...args) => fn.bind(null, ...args);

export const debug = (...args) => {
  if (window && window.sessionStorage && window.sessionStorage.getItem('DEBUG')) {
    console.log.apply(console, args);
  }
}
