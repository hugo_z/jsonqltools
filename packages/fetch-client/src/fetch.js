// this will be the fetch interface
import {
  readContract,
  storeContract,
  createHeaders,
  baseHeader,
  getUrl
} from './helpers';
import merge from 'lodash.merge';
/**
 * we need to create this from the outside
 * for each call
 * @return {object} signal for control
 */
export function createAbortController() {
  const controller = new AbortController();
  return {
    controller,
    signal: controller.signal
  };
}

/**
 * @TODO get the contract
 * @param {object} config options
 * @param {object} signal for abort controller
 * @return {object} promise resolve the contract json
 * @private
 */
export function getContract(config, signal) {
  const contract = readContract(config);
  if (contract !== false) {
    return Promise.resolve(contract);
  }
  const url = getUrl(config, true);
  const options = merge({signal}, config.fetchBase, {
    headers: createHeaders({}),
    method: 'GET'
  });
  if (signal) {
    options = Object.assign({signal}, options);
  }
  return fetch(url, options)
          .then(res => {
            if (res.ok) {
              res.json();
            }
            throw new Error(res);
          })
          .then(contract => storeContract(config, contract));
}

/**
 * main query interface
 * @param {object} config passing the config options
 * @param {string} name fn to call
 * @param {object} signal for the abort controller
 * @param {array} args this get call by the fn.apply on the server side
 * @param {object} headers additional headers to add
 * @param {mixed} signal object or false
 * @return {object} promise to resolve the result data
 * @api public
 */
export function query(config, name, signal, args = [], headers = {}) {
  const url = getUrl(config);
  const options = merge({signal}, config.fetchBase, {
    method: 'POST',
    headers: createHeaders(headers),
    body: JSON.stringify({
      [name]: {
        args
      }
    })
  });
  if (signal) {
    options = Object.assign({signal}, options);
  }
  return fetch(url, options)
          .then(res => {
            if (res.ok) {
              return res.json();
            }
            throw new Error(res);
          });
}

/**
 * main mutation interface
 * @param {object} config options
 * @param {string} name fn to call
 * @param {object} signal for abort controller
 * @param {object} payload to save
 * @param {object} conditions to use for backend
 * @param {object} headers additional headers
 * @param {mixed} signal object or false
 * @return {object} promise
 * @api public
 */
export function mutation(config, name, signal, payload = {}, conditions = {}, headers = {}) {
  const url = getUrl(config);
  const options = merge({signal}, config.fetchBase, {
    method: 'PUT',
    headers: createHeaders(headers),
    body: JSON.stringify({
      [name]: {
        payload,
        conditions
      }
    })
  });
  if (signal) {
    options = Object.assign({signal}, options);
  }
  return fetch(url, options)
          .then(res => {
            if (res.ok) {
              return res.json();
            }
            throw new Error(res);
          });
}
