# JSONql client (superagent)

Pure fetch client for jsonql, if your environment does not support fetch out of the box, you have to install polyfill to get this to work.

## Installation

```sh
$ npm install jsonql-fetch --save
```

or

```sh
$ yarn add jsonql-fetch
```

## Usage

We recommend you use the latest ES6+ with module bundler tools like [rollup](https://rollupjs.org/guide/en).

Here is a recommended setup.

```js
// First create a file call client.js
import jsonqlFetch from 'jsonql-fetch';
// more about this later
import { config, contract } from './options';
// The jsonqlClient actually return a promise
const generator = async () =>
  return await jsonqlFetch({
    host: config.host,
    contract
  });
}
const client = generator();
export default client;
```

Now in the other places where you need to use it.

```js
import client from './client';
const { query, mutation, auth } = client;
// This is a built in resolver, it will always be available
query.helloWorld().then(msg => console.log(msg));
```

As of the beta relase of the [jsonql-koa](https://www.npmjs.com/package/jsonql-koa). It support contract lock down.
So you will have to provide a key (optional), value pair.

Go back to your client.js

```js
// First create a file call client.js
import jsonqlFetch from 'jsonql-fetch';
// more about this later
import config from './config.json';
import contract from './contract.json';
// The jsonqlClient actually return a promise
const generator = async () =>
  return await jsonqlClient({
    host: config.host,
    contractKey: 'A_KEY_YOU_NEED_TO_SEND',
    // contractKeyName: if you use a custom name then you need to provide this as well
    contract
  });
}
const client = generator();
export default client;
```

Also its recommended to provide a static contract file, instead of using the dynamic option.
We are currently working on a command line client for you grab the contract, also integrate into
some of the popular work flow.

More to come.

## Server side

Please check out Node.js Koa server implementation [jsonql-koa](https://www.npmjs.com/package/jsonql-koa).

---

MIT (c) 2018 https://to1source.cn
in collaboration with https://newbran.ch
