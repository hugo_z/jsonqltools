const { join } = require('path');
const base = join(__dirname, '..', '..', '..','koa', 'tests', 'fixtures');
const options = require('./options.json');
const port = options.server.port;
const serverIoCore = require('server-io-core');
const jsonqlMiddleware = require(join(__dirname, '..', '..', '..', 'koa', 'index'));

module.exports = function(config = {}) {
  const { stop } = serverIoCore({
    webroot: __dirname,
    open: false,
    debugger: false,
    port: port,
    reload: false,
    middlewares: [
      jsonqlMiddleware(Object.assign({
        resolverDir: join(base, 'resolvers'),
        contractDir: join(base, 'contracts')
      }, config))
    ]
  });
  return stop;
}
