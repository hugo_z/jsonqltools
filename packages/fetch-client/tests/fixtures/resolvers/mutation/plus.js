const debug = require('debug')('jsonql-client:mutation:plus');
module.exports = function(payload, conditions) {
  debug('mutation.plus called', payload);
  const result = payload.a + payload.b;
  debug('result is', result);
  return result;
}
