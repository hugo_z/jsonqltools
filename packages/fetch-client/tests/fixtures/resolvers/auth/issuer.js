const debug = require('debug')('jsonql-client:auth:issuer');

module.exports = function(key) {
  debug('got a key', key);
  return 123456 + key;
}
