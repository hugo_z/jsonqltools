const debug = require('debug')('jsonql-client:auth:validator');

module.exports = function(key) {
  console.log('got a key', key);
  if (key) {
    return key.substr(0, 3) === '123';
  }
  return false;
}
