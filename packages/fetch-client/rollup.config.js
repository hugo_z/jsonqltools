/**
 * Rollup config
 */
// import fs from 'fs';
import { join } from 'path';
import buble from 'rollup-plugin-buble';
import uglify from 'rollup-plugin-uglify';

import replace from 'rollup-plugin-replace';
import commonjs from 'rollup-plugin-commonjs';

import nodeResolve from 'rollup-plugin-node-resolve';
import nodeGlobals from 'rollup-plugin-node-globals';
import builtins from 'rollup-plugin-node-builtins';
// support async functions
import async from 'rollup-plugin-async';

const env = process.env.NODE_ENV;

let plugins = [
  buble({
    objectAssign: 'Object.assign'
  }),
  nodeResolve({
    jsnext: true,
    main: true,
    browser: true
  }),
  commonjs(),
  nodeGlobals(),
  builtins(),
  async(),
  (env === 'prod') && replace({ 'process.env.NODE_ENV': JSON.stringify('production') }),
  (env === 'prod') && uglify()
];

const file = env === 'prod' ? 'jsonql-fetch.js' : 'jsonql-fetch.iife.js';

let config = {
  input: join(__dirname, 'src', 'index.js'),
  output: {
    name: 'jsonqlClient',
    file: join(__dirname, 'lib', file),
    format: env === 'prod' ? 'umd' : 'iife',
    sourcemap: true, // env !== 'prod',
    globals: {
      'promise-polyfill': 'Promise'
    }
  },
  external: [
    'Promise',
    'promise-polyfill'
  ],
  plugins: plugins
};

export default config;
