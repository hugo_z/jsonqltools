import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import angular.material animation module globally
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';

// other components
import { AppComponent } from './app.component';



@NgModule({
  declarations: [

    AppComponent
  ],
  imports: [
    MatToolbarModule,
    BrowserModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
