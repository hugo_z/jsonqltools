// this is not really for end user to use, instead if you are developing
// your jsonql compatible tool, grab this and get all the constants
// we are using everywhere
const EXT = 'js'; // we might do a ts in the future
const TS_EXT = 'ts';

const HELLO = 'Hello world!';
// the core stuff to id if it's calling with jsonql
const JSONQL_PATH = 'jsonql';
// according to the json query spec
const CONTENT_TYPE = 'application/vnd.api+json';
const DEFAULT_HEADER = {
  'Accept': CONTENT_TYPE,
  'Content-Type': CONTENT_TYPE
};

const INDEX = 'index';
const DEFAULT_TYPE = 'any';
// contract file names
const DEFAULT_FILE_NAME = 'contract.json';
const PUBLIC_FILE_NAME = 'public-contract.json';
// type of resolvers
const QUERY_NAME = 'query';
const MUTATION_NAME = 'mutation';
const RESOLVER_TYPES = [QUERY_NAME, MUTATION_NAME];
// methods allow
const API_REQUEST_METHODS = ['POST', 'PUT'];
const CONTRACT_REQUEST_METHODS = ['GET', 'HEAD'];
// for  contract-cli
const KEY_WORD = 'continue';
// author
const AUTH_TYPE = 'auth';
const ISSUER_NAME = 'issuer';
const VALIDATOR_NAME = 'validator';
const AUTH_TYPE_METHODS = [ ISSUER_NAME, VALIDATOR_NAME ];
const AUTH_HEADER = 'Authorization';
const AUTH_CHECK_HEADER = 'authorization'; // this is for checking so it must be lowercase
const BEARER = 'Bearer';
// for client use
const CLIENT_STORAGE_KEY = 'storageKey';
const CLIENT_AUTH_KEY = 'authKey';
// contract key
const CONTRACT_KEY_NAME = 'cvKey';
// directories
const DEFAULT_RESOLVER_DIR = 'resolvers';
const DEFAULT_CONTRACT_DIR = 'contracts';
// @TODO is this in use?
const CLIENT_CONFIG_FILE = '.clients.json';
// for validation
const NUMBER_TYPE = 'number';
const NUMBER_TYPES = ['int', 'integer', 'float', 'double'];
// supported types
const SUPPORTED_TYPES = ['number', 'string', 'boolean', 'array', 'object', 'any'];
const ARRAY_TYPE_LFT = 'array.<';
const ARRAY_TYPE_RGT = '>';
// export
module.exports = {
  HELLO,
  EXT,
  TS_EXT,
  INDEX,
  DEFAULT_TYPE,
  DEFAULT_FILE_NAME,
  PUBLIC_FILE_NAME,
  QUERY_NAME,
  MUTATION_NAME,
  RESOLVER_TYPES,
  API_REQUEST_METHODS,
  CONTRACT_REQUEST_METHODS,
  CONTENT_TYPE,
  KEY_WORD,
  AUTH_TYPE,
  ISSUER_NAME,
  VALIDATOR_NAME,
  AUTH_TYPE_METHODS,
  AUTH_HEADER,
  AUTH_CHECK_HEADER,
  BEARER,
  JSONQL_PATH,
  CLIENT_STORAGE_KEY,
  CLIENT_AUTH_KEY,
  CONTRACT_KEY_NAME,
  DEFAULT_RESOLVER_DIR,
  DEFAULT_CONTRACT_DIR,
  DEFAULT_HEADER,
  CLIENT_CONFIG_FILE,
  NUMBER_TYPE,
  NUMBER_TYPES,
  SUPPORTED_TYPES,
  ARRAY_TYPE_LFT,
  ARRAY_TYPE_RGT
};
