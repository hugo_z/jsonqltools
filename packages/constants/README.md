[![NPM](https://nodei.co/npm/jsonql-constants.png?compact=true)](https://npmjs.org/package/jsonql-constants)

# jsonql-constants

This is a module that just export all the share constant use across all the
[json:ql](https://jsonql.org) tools.

Use it only when you want to develop your json:ql compatible tool with javascript.

## constants

```js
HELLO,
EXT,
TS_EXT,
INDEX,
DEFAULT_TYPE,
DEFAULT_FILE_NAME,
PUBLIC_FILE_NAME,
QUERY_NAME,
MUTATION_NAME,
RESOLVER_TYPES,
API_REQUEST_METHODS,
CONTRACT_REQUEST_METHODS,
CONTENT_TYPE,
KEY_WORD,
AUTH_TYPE,
ISSUER_NAME,
VALIDATOR_NAME,
AUTH_TYPE_METHODS,
AUTH_HEADER,
AUTH_CHECK_HEADER,
BEARER,
JSONQL_PATH,
CLIENT_STORAGE_KEY,
CLIENT_AUTH_KEY,
CONTRACT_KEY_NAME,
DEFAULT_RESOLVER_DIR,
DEFAULT_CONTRACT_DIR,
DEFAULT_HEADER,
SUPPORTED_TYPES,
NUMBER_TYPES
```

---

You can also use `jsonql-constants/constants.json` to import this variables.

---

MIT

to1source / newbran ltd (c) 2019
