// it's better to do them this way

export const ARRAY_TYPE = 'array';
export const OBJECT_TYPE = 'object';
export const STRING_TYPE = 'string';
export const BOOLEAN_TYPE = 'boolean';
export const NUMBER_TYPE = 'number';

export const LFT = 'array.<';
export const RHT = '>';
