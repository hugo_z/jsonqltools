// main export
import checkIsAny from './any';
import checkIsArray from './array';
import checkIsBoolean from './boolean';
import checkIsNumber from './number';
import checkIsObject from './object';
import checkIsString from './string';

export {
  checkIsAny,
  checkIsString,
  checkIsBoolean,
  checkIsNumber,
  checkIsArray,
  checkIsObject
};
