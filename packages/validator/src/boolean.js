// check for boolean
import { isBoolean } from 'lodash-es';
/**
 * @param {boolean} value expected
 * @return {boolean} true if OK
 */
export default function checkIsBoolean(value) {
  return isBoolean(value);
}
