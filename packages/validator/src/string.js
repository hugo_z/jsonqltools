// validate string type
import { isString, trim } from 'lodash-es';
/**
 * @param {string} value expected value
 * @return {boolean} true if OK
 */
export default function checkIsString(value) {
  return (trim(value) !== '') ? isString(value) : false;
}
