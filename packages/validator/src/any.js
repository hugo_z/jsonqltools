// validate any thing only check if there is something
import { isNull, trim } from 'lodash-es';
/**
 * @param {*} value the value
 * @param {boolean} [checkNull=true] strict check if there is null value
 * @return {boolean} true is OK
 */
export default function checkIsAny(value, checkNull = true) {
  if (value !== '' && trim(value) !== '' && value !== undefined) {
    if (checkNull === false || (checkNull === true && !isNull(value))) {
      return true;
    }
  }
  return false;
}
