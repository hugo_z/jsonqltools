// validator numbers
import { NUMBER_TYPES } from 'jsonql-constants';
import { isNaN } from 'lodash-es';

/**
 * @param {number} value expected value
 * @return {boolean} true if OK
 */
export default function checkIsNumber(value) {
  return !isNaN( parseFloat(value) );
}
