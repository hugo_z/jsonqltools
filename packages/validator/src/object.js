// validate object type
import { isPlainObject } from 'lodash-es';
import combineFn from './combine';
/**
 * @TODO if provide with the keys then we need to check if the key:value type as well
 * @param {object} value expected
 * @param {array} [keys=null] if it has the keys array to compare as well
 * @return {boolean} true if OK
 */
export default function checkIsObject(value, keys=null) {
  if (isPlainObject(value)) {
    if (keys === null) {
      return true;
    }

  }
  return false;
}
