// imports
// these two only check for primitive or we don't check at all?
// import checkIsArray from './array';
// import checkIsObject from './object';
// primitive types
import checkIsNumber from './number';
import checkIsString from './string';
import checkIsBoolean from './boolean';
import checkIsAny from './any';
import {
  NUMBER_TYPE,
  STRING_TYPE,
  BOOLEAN_TYPE
  // ARRAY_TYPE,
  // OBJECT_TYPE
} from './constants';

/**
 * this is a wrapper method to call different one based on their type
 * @param {string} type to check
 * @return {function} a function to handle the type
 */
export default function combineFn(type) {
  switch (type) {
    case NUMBER_TYPE:
      return checkIsNumber;
    case STRING_TYPE:
      return checkIsString;
    case BOOLEAN_TYPE:
      return checkIsBoolean;
    /* case ARRAY_TYPE:
      return checkIsArray;
    case OBJECT_TYPE:
      return checkIsObject; */
    default:
      return checkIsAny;
  }
}
