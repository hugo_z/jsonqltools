// validate array type
import { isArray, trim } from 'lodash-es';
import combineFn from './combine';
import debug from 'debug';
const _debug = debug('jsonql-params-validator:checkIsArray');
/**
 * @param {array} value expected
 * @param {string} [type=''] pass the type if we encounter array.<T> then we need to check the value as well
 * @return {boolean} true if OK
 */
export default function checkIsArray(value, type='') {
  if (isArray(value)) {
    if (trim(type)==='') {
      return true;
    }
    // we test it in reverse
    const c = value.filter( v => !combineFn(type)(v) );
    _debug(`filtered by ${type} values`, c);
    return !(c.length > 0);
  }
  return false;
}
