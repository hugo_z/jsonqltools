const test = require('ava');
const debug = require('debug')('jsonql-params-validator:combine-test');
const {
  checkIsArray,
  checkIsObject
} = require('../src');

test('Test with a complex array type check', t => {
  t.true(checkIsArray([1,2,3], 'number'), 'Pass an array of numbers');
  t.false(checkIsArray(['1',2,'3'], 'string'), 'Pass an array of mixed values');
});

test('Test with complex object type check', t => {

  

});


test.todo('Combine api test using contract.json');
