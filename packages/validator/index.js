// export

import {
  checkIsArray
} from './src';

/**
 * validator main interface
 * @param {array} args the arguments pass to the method call
 * @param {array} params from the contract for that method
 * @return {boolean|array} true on success, or an array of failed parameter and reasons
 */
/*
export default function main(args, params) {

}
*/

const first = checkIsArray([1,2,3], 'number');
const second = checkIsArray([1,'2','3'], 'string');

console.log('first', first);
console.log('second', second);
