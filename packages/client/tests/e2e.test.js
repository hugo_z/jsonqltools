// This will hook up to the jsonql-koa for proper e2e testing including the security cert test
const { join } = require('path');
const { inspect } = require('util');
const test = require('ava');
const superagent = require('superagent');
// const Promise = require('promise-polyfill');
const debug = require('debug')('jsonql:client:test');
const Window = require('window');
const window = new Window();
window.superagent = superagent;
window.localStorage = {};
window.sessionStorage = {};
// window.Promise = Promise;
// window['promise-polyfill'] = Promise;
const JsonqlClient = require(join(__dirname, '..', 'lib' , 'jsonql-client.js'));
const options = require('./fixtures/options.json');
const server = require('./fixtures/server');

test.before(t => {
  t.context.stop = server({
    contractKey: options.auth.contractKey
  });
});

test.after(t => {
  t.context.stop();
});

test('Try to fetch the contract from the server when its lock by a key', async (t) => {
  const client = await JsonqlClient({
    hostname: ['http://localhost', options.server.port].join(':'),
    contractKey: options.auth.contractKey
  });

  const { jsonqlClient } = client;
  const contract = jsonqlClient.getContract();

  debug('received contract', contract);

  t.is(true, typeof contract === 'object');
});
