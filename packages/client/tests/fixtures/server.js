const { join } = require('path');
const options = require('./options.json');
const port = options.server.port;
const serverIoCore = require('server-io-core');
const jsonqlMiddleware = require(join(__dirname, '..', '..', '..', 'koa', 'index'));

module.exports = function(config = {}) {
  const { stop } = serverIoCore({
    webroot: [
      __dirname,
      join(__dirname, '..', '..', 'node_modules'),
      join(__dirname, '..', '..', 'lib')
    ],
    open: true,
    debugger: true,
    port: port,
    reload: false,
    middlewares: [
      jsonqlMiddleware(Object.assign({
        // auth: true,
        resolverDir: join(__dirname, 'resolvers'),
        contractDir: join(__dirname, 'contracts')
      }, config))
    ]
  });
  return stop;
}
