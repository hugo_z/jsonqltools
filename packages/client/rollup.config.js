/**
 * Rollup config
 */
import fs from 'fs';
import { join } from 'path';
import buble from 'rollup-plugin-buble';
import uglify from 'rollup-plugin-uglify';

import replace from 'rollup-plugin-replace';
import commonjs from 'rollup-plugin-commonjs';

import nodeResolve from 'rollup-plugin-node-resolve';
import nodeGlobals from 'rollup-plugin-node-globals';
import builtins from 'rollup-plugin-node-builtins';

// support async functions
import async from 'rollup-plugin-async';

if (fs.existsSync('./lib/main.js.map')) {
  fs.unlinkSync('./lib/main.js.map');
};

const env = process.env.NODE_ENV;

let plugins = [
  buble({
    objectAssign: 'Object.assign'
  }),
  nodeResolve({
    jsnext: true,
    main: true,
    browser: true
  }),
  commonjs({
    include: 'node_modules/**'
  }),
  nodeGlobals(),
  builtins(),
  async(),
  (env === 'prod' || env === 'esm') && replace({ 'process.env.NODE_ENV': JSON.stringify('production') }),
  (env === 'prod' || env === 'esm') && uglify()
];

const file = env === 'esm' ? 'jsonql-client.js' : 'jsonql-client.iife.js';

console.log('outfile is %s', file);

let config = {
  input: join(__dirname, 'src', 'index.js'),
  output: {
    name: 'jsonqlClient',
    file: join(__dirname, 'lib', file),
    format: env === 'esm' ? 'umd' : 'iife',
    sourcemap: true, // env !== 'prod',
    globals: {
      superagent: 'superagent',
      'promise-polyfill': 'Promise'
    }
  },
  external: [
    'Promise',
    'promise-polyfill',
    "superagent",
    "handlebars",
    "tty"
  ],
  plugins: plugins
};

export default config;
