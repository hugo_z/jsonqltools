/**
 * Create the jsonql client
 * @TODO we should have a before submit call to filter out items not in the contract
 */

import superagent from 'superagent';
// import Promise from 'promise-polyfill';
import merge from 'lodash.merge';
import JsonqlClient from './base';
const MUTATION_ARGS = ['name', 'payload', 'conditions'];
// main
export default class JsonqlSuperagentClient extends JsonqlClient {

  /**
   * @param {object} config configuaration
   */
  constructor(config = {}) {
    super(config);

    this.request = superagent;
    this.mutationArgs = MUTATION_ARGS;
  }

  /**
   * try to figure out the error message return from server
   */
  __extractError(res) {
    if (res.body) {
      if (res.body.error) {
        return res.body.error;
      }
      return res.body;
    }
    return res;
  }

  /**
   * @TODO get the contract
   * @param {object} header
   * @return {object} promise resolve the contract json
   * @private
   */
   getContract(header = {}) {
     const c = this.__readContract();
     if (c !== false) {
       return Promise.resolve(c);
     }
     return this.request
      .get([this.opts.hostname, this.opts.jsonqlPath].join(''))
      .query(this.__getAuth())
      .timeout(this.opts.timeout)
      .set(merge(this.baseHeader, header))
      .then(res => {
        if (res.ok && res.body) {
          return res.body;
        }
        throw new Error(this.__extractError(res));
      })
      .then(this.__storeContract.bind(this));
  }

  /**
   * main query interface
   * @param {string} name fn to call
   * @param {array} args this get call by the fn.apply on the server side
   * @param {object} headers additional headers to add
   * @return {object} promise to resolve the result data
   * @api public
   */
  query(name, args = [], headers = {}) {
    return this.request
      .post([this.opts.hostname, this.opts.jsonqlPath].join(''))
      .query(this.__cacheBurst())
      .timeout(this.opts.timeout)
      .set(this.__createHeaders(headers))
      .send({
        [name]: {args}
      })
      .then(res => {
        if (res.ok && res.body.data) {
          return res.body;
        }
        throw new Error(this.__extractError(res));
      });
  }

  /**
   * main mutation interface
   * @param {string} name fn to call
   * @param {object} payload to save
   * @param {object} conditions to use for backend
   * @param {object} headers additional headers
   * @return {object} promise
   * @api public
   */
  mutation(name, payload = {}, conditions = {}, headers = {}) {
    return this.request
      .put([this.opts.hostname, this.opts.jsonqlPath].join(''))
      .query(this.__cacheBurst())
      .timeout(this.opts.timeout)
      .set(this.__createHeaders(headers))
      .send({
        [name]: {
          payload,
          conditions
        }
      })
      .then(res => {
        if (res.ok && res.body.data) {
          return res.body;
        }
        throw new Error(this.__extractError(res));
      });
  }
}
