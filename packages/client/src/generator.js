
/**
 * handle the return data
 * @param {object} result return from server
 * @return {object} strip the data part out, or if the error is presented
 */
const resultHandler = result => {
  if (result.data && !result.error) {
    return result.data;
  }
  throw new Error(result.error);
}

/**
 * @param {object} contract the contract
 * @param {object} jsonqlInstance jsonql class instance
 * @param {object} config options
 * @return {object} constructed functions call
 */
const generator = (contract, jsonqlInstance, config) => {
  let obj = {query: {}, mutation: {}, auth: {}};
  // process the query first
  for (let queryFn in contract.query) {
    // to keep it clean we use a param to id the auth method
    // const fn = (_contract.query[queryFn].auth === true) ? 'auth' : queryFn;
    // generate the query method
    obj.query[queryFn] = (...args) => {
      const params = contract.query[queryFn].params;
      const _params = params.map((param, i) => args[i]);
      // debug('query', queryFn, _params);
      // the +1 parameter is the extra headers we want to pass
      const header = args[params.length] || {};
      // @TODO validate against the type
      return jsonqlInstance.query
        .apply(jsonqlInstance, [queryFn, _params, header])
        .then(resultHandler);
    };
  }
  // process the mutation, the reason the mutation has a fixed number of parameters
  // there is only the payload, and conditions parameters
  // plus a header at the end
  for (let mutationFn in contract.mutation) {
    obj.mutation[mutationFn] = (payload, conditions) => {
      const header = {};
      return jsonqlInstance.mutation
        .apply(jsonqlInstance, [mutationFn, payload, conditions, header])
        .then(resultHandler);
    };
  }
  // there is only one call issuer we want
  if (contract.auth && contract.auth.issuer) {
    obj.auth = (...args) => {
      const params = contract.auth.issuer.params.map((p, i) => args[i]);
      // debug('auth', params);
      const header = args[params.length] || {};
      return jsonqlInstance.query.apply(jsonqlInstance, ['issuer', params, header])
        .then(result => {
          if (result.data) {
            return jsonqlInstance.token = result.data;
          }
          throw new Error(result.error || 'Auth failed');
        });
    }
  }
  // store this once again and export it
  if (obj.returnInstance) {
    obj.jsonqlClient = jsonqlInstance;
  }
  // this is handy if we need the same token to validate against socket
  if (obj.allowReturnRawToken) {
    obj.getToken = () => jsonqlInstance.__getRawAuthToken();
  }
  // output
  return obj;
};

export default generator;
