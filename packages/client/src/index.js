// main interface
import generator from './generator';
import JsonqlSuperagentClient from './main';
/**
 * @param {object} jsonqlInstance the init instance of jsonql client
 * @param {object} contract the static contract
 * @return {object} contract may be from server
 */
const getContract = (jsonqlInstance, contract = {}) => {
  if (contract && contract.query && contract.mutation) {
    return Promise.resolve(contract);
  }
  return jsonqlInstance.getContract();
};

/**
 * The final interface using the contract to generate callable methods
 * @param {object} config for the jsonql instance
 * @param {object} contract the json file it's empty object then we call the get contract
 * @return {object} key value path of callable methods
 */
export default function(config = {}) {
  const jsonql = new JsonqlSuperagentClient(config);

  return getContract(
    jsonql,
    config.contract || false
  ).then(
    _contract => generator(_contract, jsonql, config)
  );
}
