const test = require('ava');
const server = require('./fixtures/server-with-auth');
const debug = require('debug')('jsonql-node-client:test:auth');
const { display } = require('../src/utils');
const { join } = require('path');
const { contractKey, loginToken, token } = require('./fixtures/options');
const fs = require('fs');
const contractDir =join(__dirname,'fixtures','contract','client-with-auth');
const { PUBLIC_FILE_NAME } = require('jsonql-constants');
const nodeClient = require('../index');

test.before(async (t) => {
  const { stop } = await server();
  t.context.stop = stop;
});

test.after(t => {
  t.context.stop();
});

test('Have to run this one test to do it all for all the auth function', async (t) => {

  const client = await nodeClient({
    hostname:'http://localhost:8889',
    contractDir,
    contractKey,
  });

  t.is(true, fs.existsSync(join(contractDir, PUBLIC_FILE_NAME)));

  const { auth } = client;

  t.is(true, typeof auth === 'function');

  const result = await auth(loginToken);

  t.is(token, result);

});
