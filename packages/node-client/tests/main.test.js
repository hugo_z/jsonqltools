const test = require('ava');
const { join } = require('path');
const server = require('./fixtures/server');
const options = require('./fixtures/options');
const nodeClient = require('../index');

test.before(async t => {
  const { stop } = await server();
  t.context.stop = stop;
});

test.after(async t => {
  t.context.stop();
});

test('Should able to say Hello world!' , async t => {
  const client = await nodeClient({
    hostname:'http://localhost:8888',
    contractDir: join(__dirname,'fixtures','contract','client')
  });

  console.log(client.query);

  const result = await client.query.helloWorld();
  t.is('Hello world!', result);
});
