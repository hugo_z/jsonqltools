const server = require('server-io-core');
const jsonql = require('../../../koa');
const options = require('./options');
const { join } = require('path');
module.exports = function() {
  return server({
    port: 8888,
    debug: false,
    open: false,
    middlewares: [
      jsonql({
        resolverDir: join(__dirname,'resolvers'),
        contractDir: join(__dirname,'contract','server')
      })
    ]
  });
}
