const server = require('server-io-core');
const jsonql = require('../../../koa');
const { contractKey } = require('./options');
const { join } = require('path');

module.exports = function() {
  return server({
    port: 8889,
    debug: false,
    open: false,
    middlewares: [
      jsonql({
        contractKey,
        resolverDir: join(__dirname,'resolvers'),
        contractDir: join(__dirname,'contract','server-with-auth'),
        auth: true
      })
    ]
  });
}
