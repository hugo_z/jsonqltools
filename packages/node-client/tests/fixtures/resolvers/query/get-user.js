const debug = require('debug')('jsonql-node-client:test:get-user');
const { users, msg } = require('../options');
module.exports = function(id) {
  return users[ parseInt(id, 10) ] || msg;
}
