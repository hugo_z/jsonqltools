const debug = require('debug')('jsonql-node-client:test:issuesr');
const { loginToken, token } = require('../../options');

module.exports = function(credential) {
  return credential === loginToken ? token : false;
}
