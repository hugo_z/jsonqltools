const debug = require('debug')('jsonql-node-client:test:issuer');
const { token } = require('../options');

module.exports = function(userToken) {
  return token === userToken;
}
