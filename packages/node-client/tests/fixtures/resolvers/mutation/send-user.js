const debug = require('debug')('jsonql-node-client:test:send-user');
const merge = require('lodash.merge');

module.exports = function(payload, conditions) {
  return merge({timestamp: Date.now()}, payload, conditions);
}
