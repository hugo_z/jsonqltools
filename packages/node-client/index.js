// this is the top level of app
const { getContract, generator } = require('./src/generator');
const JsonqlRequestClient = require('./src/main');
const { display } = require('./src/utils');
const debug = require('debug')('jsonql-node-client:index');
// finally
/**
 * The config takes two things
 * hostname, contractDir
 * if there is security feature on then the contractKey
 */
module.exports = function(config = {}) {
  debug('Running node-client with this options', config);
  // init
  const jsonqlInstance = new JsonqlRequestClient(config);
  // run
  return getContract(jsonqlInstance, config.contract || false)
    .then(contract => generator(jsonqlInstance, contract));
}
