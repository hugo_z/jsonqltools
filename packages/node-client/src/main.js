/* global fetch */
/**
 * Fetch version without superagent
 */
const request = require('request');
const fsx = require('fs-extra');
const { display } = require('./utils');
const debug = require('debug')('jsonql-node-client:main');
const JsonqlClient = require('./base');
// main
class JsonqlRequestClient extends JsonqlClient {

  constructor(config = {}) {
    super(config);
  }

  // just a wrapper
  getContract() {
    return this.__readContract().then(result => {
      if (typeof result === 'string') {
        return fsx.readJsonSync(result);
      }
      return result;
    });
  }

  /**
   * wrapper method for the request
   * @param {string} method POST PUT
   * @param {object} payload what to send
   * @param {object} headers extra headers
   */
  __requestWrapper(method, payload, headers) {
    debug('sending payload', display(payload,1));
    return new Promise((resolver, rejecter) => {
      request({
        json: true,
        url: this.__url__,
        headers: this.__createHeaders(headers),
        qs: this.__cacheBurst(), //it's not necessary but just keep it
        method: method,
        body: payload
      }, (error, response, body) => {
        debug('response from query', display(response));
        if (error) {
          debug('an error occured', display(error));
          return rejecter(error);
        }
        debug('result body', display(body));
        // const result = typeof body === 'string' ? JSON.parse(body) : body;
        resolver(body);
      })
    });
  }

  /**
   * main query interface
   * @param {string} name fn to call
   * @param {object} args to get around the name paramter problem
   * @param {object} headers additional headers to add
   * @return {object} promise to resolve the result data
   * @api public
   */
  query(name, args = [], headers = {}) {
    const payload = { [name]: {args} };
    return this.__requestWrapper('POST', payload, headers);
  }

  /**
   * main mutation interface
   * @param {string} name fn to call
   * @param {object} payload to save
   * @param {object} conditions to use for backend
   * @param {object} headers additional headers
   * @return {object} promise
   * @api public
   */
  mutation(name, payload={}, conditions={}, headers={}) {
    const payload = {[name]: {payload, conditions}};
    return this.__requestWrapper('PUT', payload, headers);
  }
}

module.exports = JsonqlRequestClient;
