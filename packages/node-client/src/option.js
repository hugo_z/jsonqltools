const { join } = require('path');
const {
  JSONQL_PATH,
  CONTENT_TYPE,
  CLIENT_STORAGE_KEY,
  CLIENT_AUTH_KEY,
  CONTRACT_KEY_NAME,
  DEFAULT_RESOLVER_DIR,
  PUBLIC_FILE_NAME,
  DEFAULT_HEADER
} = require('jsonql-constants');
// exports
module.exports = {
  hostname: '', // required the hostname
  jsonqlPath: JSONQL_PATH, // The path on the server
  contentType: CONTENT_TYPE, // the header
  useLocalstorage: true, // should we store the contract into localStorage
  storageKey: CLIENT_STORAGE_KEY, // the key to use when store into localStorage
  authKey: CLIENT_AUTH_KEY, // the key to use when store into the sessionStorage
  contract: false,
  contractKey: false, // if the server side is lock by the key you need this
  contractKeyName: CONTRACT_KEY_NAME, // same as above they go in pairs
  contractDir: join(process.cwd(), DEFAULT_RESOLVER_DIR),
  contractFileName: PUBLIC_FILE_NAME,
  // functions
  storeAuthToken: false,
  getAuthToken: false,
  defaultHeader: DEFAULT_HEADER
};
