/**
 * Base class to inherit from
 * @TODO we should move all the main back here
 * because the main interface should be a generator based on the contract
 */
const jsonqlContract = require('jsonql-contract');
const { join } = require('path');
const fs = require('fs');
const fsx = require('fs-extra');
const merge = require('lodash.merge');
const defaultOptions = require('./option');

class JsonqlClient {

  constructor(config = {}) {
    this.opts = merge({}, defaultOptions, config);
    // we only need to store key in memory
    this.__store__ = {};
    this.__url__ = [this.opts.hostname, this.opts.jsonqlPath].join('/');
  }

  /**
   * computed property
   * @return {object} for the accepted header
   */
  get baseHeader() {
    const { contentType } = this.opts;
    return {
      'Accept': contentType,
      'Content-Type': contentType
    };
  }

  /**
   * setter for the issuer method to use
   * @param {string} token to store
   * @return {string} on success
   */
  set token(token) {
    return this.__storeAuthToken(token);
  }

  /**
   * @return {object} cache burster
   * @private
   */
  __cacheBurst() {
    return {_cb: (new Date()).getTime() };
  }

  /**
   * @param {object} header to set
   * @return {object} combined header
   * @private
   */
  __createHeaders(header = {}) {
    const authHeader = this.__getAuthToken();
    return merge({}, this.baseHeader, header, authHeader);
  }

  /**
   * Return the stored contract if any
   * @TODO need an option to compare the time and check with the server
   * @return {object} return a promise to resolve it
   */
  __readContract() {
    const { contract } = this.opts;
    // first check if we have a contract already in memory
    if (contract && typeof contract === 'object') {
      return Promise.resolve(contract);
    }
    // check if there is a contract store locally
    const file = join(this.opts.contractDir, this.opts.contractFileName);
    if (fs.existsSync(file)) {
      return Promise.resolve(fsx.readJsonSync(file));
    }
    // now grab it from remote
    let url = this.__url__;
    if (this.opts.contractKey !== false) {
      url += `?${this.opts.contractKeyName}=${this.opts.contractKey}`;
    }
    // use the remote method to get it
    return jsonqlContract({ remote: url, out: this.opts.contractDir })
  }

  /**
   * Store the auth token into session storage
   * @return {string} token
   */
  __storeAuthToken(token) {
    if (this.opts.storeAuthToken && typeof this.opts.storeAuthToken === 'function') {
      return this.opts.storeAuthToken(token);
    }
    this.__store__[this.opts.authKey] = token;
    return token;
  }

  /**
   * Get the stored auth token from session storage
   * @return {string} auth token
   */
  __getAuthToken() {
    if (this.opts.getAuthToken && typeof this.opts.getAuthToken === 'function') {
      return this.opts.getAuthToken();
    }
    return this.__store__[this.opts.authKey];
  }

  /**
   * When we have the contractKey setup then we need to include this into heading
   * @return {object} combine object
   */
  __getAuth() {
    const extraParams = this.__cacheBurst();
    if (this.opts.contractKey === false) {
      return extraParams;
    }
    const contractAuth = {[this.opts.contractKeyName]: this.opts.contractKey};
    // debug('let see the contract key value pair', contractAuth);
    return merge({}, contractAuth, extraParams);
  }
}

// export
module.exports = JsonqlClient;
