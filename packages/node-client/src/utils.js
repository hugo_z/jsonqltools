const { inspect } = require('util');

exports.display = (data, full = false) => (
  full ? inspect(data, false, null, true) : data.toString()
);
