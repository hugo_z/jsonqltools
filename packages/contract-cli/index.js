// now when ever some one do contract = require('jsonql-contract') will include this one
const { generator, handler, remote } = require('./lib');
const { KEY_WORD } = require('jsonql-constants');
const debug = require('debug')('jsonql-contract:cli');
// main
module.exports = function(config) {
  debug('call import interface with this config', config);
  return remote(config).then(result => {
    if (result === KEY_WORD) {
      return handler(config).then(generator);
    }
    return result;
  });
}
