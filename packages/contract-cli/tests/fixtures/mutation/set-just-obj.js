// this mutation only provide the top level object with the details
/**
 * @param {object} payload the payload
 * @param {object} condition the condition to apply the mutation
 * @return {boolean} true on success
 */
module.exports = function(payload, condition) {
  // some other code
  return true;
}
