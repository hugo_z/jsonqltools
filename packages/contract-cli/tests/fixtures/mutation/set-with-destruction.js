// this one use the destruct style

/**
 * @param {object} payload
 * @param {string} payload.title
 * @param {string} payload.content
 * @param {object} condition
 * @param {number} id
 * @return {boolean} true on success
 */
module.exports = function({title, content}, {id}) {
  return true;
}
