// this one to check how to do a style[] style params

/**
 * @param {number[]} userIds array of user ids
 * @return {string[]} return a list of username
 */
module.exports = function(userIds) {
  return userIds.map(id => (id+'') + 'username');
}
