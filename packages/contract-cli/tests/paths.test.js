const test = require('ava');
const { join } = require('path');
const { handler } = require('../lib');
const dir = join(__dirname, 'fixtures');
const debug = require('debug')('jsonql-contract:test:paths');

test('Should able to have in and out', async t => {
  const args = {inDir: dir};
  const result = await handler(args);
  // debug('result', result);
  t.is(true, (result.inDir !== undefined && result.outDir !== undefined));
  t.is(dir , result.inDir);
});
