const test = require('ava');
const fs = require('fs');
const { join } = require('path');
const { inspect } = require('util');
const { generator } = require('../lib');
const inDir = join(__dirname, 'fixtures');
const outDir = join(__dirname, 'fixtures', 'tmp');
const debug = require('debug')('jsonql-contract:test:generator');
const fsx = require('fs-extra');

const baseContractFile = join(outDir, 'contract.json');
const publicContractFile = join(outDir, 'public-contract.json');

test.after(async t => {
  fsx.removeSync(baseContractFile);
  fsx.removeSync(publicContractFile);
});

test('Should able to read list of files', async t => {
  const result = await generator({inDir, outDir, raw: true});
  debug('raw output', inspect(result, false, null));
  t.is(true, result.query !== undefined);
});

test('There should be a contract.json output to the outDir', async t => {
  const result = await generator({inDir, outDir});
  t.is(true, fs.existsSync( baseContractFile ));
});

test('Should able to create a public-contract.json', async t => {
  const result = await generator({inDir, outDir, public: true});

  t.is(true, fs.existsSync(publicContractFile));
  const json = fsx.readJsonSync(publicContractFile);
  t.is(false, !!json.auth.validator);
});

/* move this into it's own file next
test.skip('Should able to read the jsdoc with correct params and returns', async t => {
  const result = await generator({inDir, outDir, useDoc: true});
  const file = join(outDir, 'public-contract.json');

});
*/
