// test with the remote option
const test = require('ava');
const server = require('../../node-client/tests/fixtures/server');
const debug = require('debug')('jsonql-contract:test:remote');
const contract = require('../index');
const { join } = require('path');
const contractDir = join(__dirname, 'contract');
const { PUBLIC_FILE_NAME } = require('jsonql-constants');
const contractFile = join(contractDir, PUBLIC_FILE_NAME);
const fsx = require('fs-extra');

test.before(async t => {
  const { stop } = await server();
  t.context.stop = stop;
});

test.after(async t => {
  t.context.stop();
  fsx.removeSync(contractFile);
});

test.skip('Should table to get a remote contract file', async t => {
  const result = await contract({
    remote: 'http://localhost:8888',
    out: contractDir
  });
  t.is(true, fsx.existsSync(contractFile));
});
