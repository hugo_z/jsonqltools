const fs = require('fs');
const debug = require('debug')('jsonql-contract:paths');
const { resolve } = require('path');

/**
 * The input from cmd and include are different and cause problem for client
 * @param {mixed} args array or object
 * @return {object} sorted params
 */
function getPaths(argv) {
  return new Promise((resolver, rejecter) => {
    const baseDir = process.cwd();
    if (argv.inDir) {
      if (argv.outDir) {
        argv.outDir = resolve(argv.outDir);
      } else {
        argv.outDir = baseDir;
      }
      argv.inDir = resolve(argv.inDir);
      return resolver(argv); // just return it
    } else if (argv._) {
      const args = argv._;
      if (args[0]) {
        return resolver({
          inDir: resolve(args[0]),
          outDir: args[1] ? resolve(args[1]) : baseDir,
          useDoc: argv.useDoc,
          // raw: argv.raw, if they are using command line raw is not available as option
          public: argv.public
        });
      }
    }
    rejecter('You need to provide at least the input path');
  });
}

/**
 * @param {object} paths in out
 * @return {object} promise
 */
function checkInputPath(paths) {
  return new Promise((resolver, rejecter) => {
    if (paths.inDir) {
      try {
        const stats = fs.lstatSync(paths.inDir);
        if (stats.isDirectory()) {
          resolver(paths);
        } else {
          rejecter(`The in path ${paths.inDir} does not existed`);
        }
      }
      catch (e) {
        rejecter(e);
      }
    }
  });
}

// main
module.exports = function(args) {
  return getPaths(args).then(checkInputPath);
}
