const generator = require('./generator');
const handler = require('./paths');
const remote = require('./remote');
// main
module.exports = {
  generator,
  handler,
  remote
};
