// porting back from jsonql-koa to grab the NODE_ENV.json and remove the files information to public use
const { join } = require('path');
const fsx = require('fs-extra');
const { merge } = require('lodash');
// const { DEFAULT_FILE_NAME } = require('jsonql-constants');
/**
 * we need to remove the file info from the contract if this is for public
 * @param {object} json contract
 * @param {boolean} auth if its not true then remove the issuer if presented
 * @return {string} contract without the file field
 */
const cleanForPublic = (json, auth=false) => {
  for (let type in json) {
    for (let fn in json[type]) {
      delete json[type][fn].file;
    }
  }
  // also if there is a validator field then delete it
  if (json.auth.validator) {
    delete json.auth.validator;
  }
  if (!auth && json.auth.issuer) {
    delete json.auth.issuer;
  }
  return json;
};

/**
 * using the NODE_ENV to check if there is extra contract file
 * @param {string} contractDir directory store contract
 * @return {object} empty object when nothing
 */
function getEnvContractFile(contractDir) {
  const nodeEnv = process.env.NODE_ENV;
  const overwriteContractFile = join(contractDir, [nodeEnv, 'json'].join('.'));
  if (fsx.existsSync(overwriteContractFile)) {
    debug('found env contract');
    return fsx.readJsonSync(overwriteContractFile);
  }
  return {};
}

/**
 * @param {object} config the original config
 * @param {object} contractJson the raw json file
 * @return {string} json
 */
module.exports = function(config, contractJson) {
  const contractDir = config.outDir;
  if (!contractDir) {
    throw new Error('Contract directory is undefined!');
  }
  const baseContract = fsx.readJsonSync(join(__dirname, 'hello-world.json'));
  // env contract file - this should not get written to file
  const extraContracts = config.raw ? getEnvContractFile(contractDir) : {};
  // export
  return cleanForPublic(
    merge(
      {},
      baseContract,
      contractJson,
      extraContracts
    )
  );
}
