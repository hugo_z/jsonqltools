// fetching the remote contract and save locally
// jsonql-contract --remote=http://hostname/jsonql
const request = require('request');
const { join, resolve } = require('path');
const fsx = require('fs-extra');
const {
  CONTENT_TYPE,
  PUBLIC_FILE_NAME,
  KEY_WORD,
  DEFAULT_HEADER
} = require('jsonql-constants');

// export it
module.exports = function(args) {
  return new Promise((resolver, rejecter) => {
    // defintely we have the remote or not here
    if (!args.remote) {
      return resolver(KEY_WORD); // allow the next chain to do stuff
    }
    // options
    const options = { url: args.remote, headers: DEFAULT_HEADER };
    // method
    function callback(error, response, body) {
      if (!error && response.statusCode == 200) {
        const contract = JSON.parse(body);
        const outFile = join( resolve(args.out) || process.cwd(), PUBLIC_FILE_NAME );
        // v1.1.4 add raw output
        if (args.raw) {
          return resolver(contract);
        }
        fsx.outputJson(outFile, contract, {spaces: 2}, err => {
          if (err) {
            return rejecter(err);
          }
          resolver(outFile);
        });
      }
    }
    request(options, callback);
  });
}
