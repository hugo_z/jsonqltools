// The core generator
const { inspect } = require('util');
const { join, basename } = require('path');
const fsx = require('fs-extra');
const fs = require('fs');
const glob = require('glob');
const {
  compact,
  trim,
  merge,
  camelCase
} = require('lodash');
const debug = require('debug')('jsonql-contract:generator');
const {
  EXT,
  INDEX,
  DEFAULT_TYPE,
  DEFAULT_FILE_NAME,
  RESOLVER_TYPES,
  AUTH_TYPE_METHODS,
  AUTH_TYPE,
  PUBLIC_FILE_NAME
} = require('jsonql-constants');
const publicContractGenerator = require('./public-contract');
// new v1.2.0 adding the jsdoc api
const { getJsdoc } = require('./jsdoc');
const {
  acornParser,
  extractReturns,
  extractParams,
  isExpression
} = require('./acorn');

/**
 * @param {string} item to check
 * @return {boolean} found losey true otherwise false
 */
const inTypesArray = function(item) {
  return RESOLVER_TYPES.filter(type => item === type).length;
}

/**
 * @param {string} type to compare
 * @param {string} file name to check
 * @return {boolean}
 */
const isAuthType = function(type, file) {
  return type === AUTH_TYPE && AUTH_TYPE_METHODS.filter(method => file === method).length;
}

/**
 * filter to get which is resolver or not
 * @param {string} inDir the base path
 * @param {string} fileType ext
 * @return {function} work out if it's or not
 */
function getResolver(inDir, fileType) {
  const indexFile = [INDEX, fileType].join('.');
  // return a function here
  return baseFile => {
    const failed = {ok: false};
    const files = compact(baseFile.replace(inDir, '').toLowerCase().split('/'));
    const ctn = files.length;
    const type = files[0];
    // we ignore all the files on the root level
    if (files.length === 1) {
      return failed;
    }
    // process files within the folder of query, mutation, auth
    const fileName = basename(files[1], '.' + fileType);
    switch (ctn) {
      case 3:
        if (inTypesArray(type) || isAuthType(type, fileName)) {
          return {
            ok: (files[ctn - 1] === indexFile),
            type: type,
            name: camelCase(fileName),
            file: baseFile
          };
        }
      case 2:
        if (inTypesArray(type) || isAuthType(type, fileName)) {
          return {
            ok: true,
            type: type,
            name: camelCase(fileName),
            file: baseFile
          };
        }
      default:
        return failed;
    }
  }
}

/**
 * The return result shouldn't be array of array
 * it should be just one level array
 * @param {array} arr unknown level of array
 * @return {mixed} depends
 */
function takeDownArray(arr) {
  if (arr.length && arr.length === 1) {
    return takeDownArray(arr[0]);
  }
  return arr;
}

/**
 * wrapper for the AST parser (using acorn)
 * @param {string} source the stringify version of the function
 * @return {object} the result object
 */
function astParser(source) {
  return acornParser(source);
}

/**
 * Control if we want to use jsdoc or not
 * @param {object} config configuration props
 * @param {object} result process result
 * @param {string} source from function
 * @param {string} name of function
 * @return {object} with description, params, returns
 */
function extractExtraProps(config, result, source, name) {
  if (config.useDoc) {
    return getJsdoc(source, name);
  }
  return {
    description: false,
    params: [],
    returns: result.body.filter(isExpression).map(tree => extractReturns(tree.expression)).filter(r => r)
  };
}

/**
 * process the files
 * @param {string} inDir base directory
 * @param {string} fileType the ext
 * @param {array} files list of files
 * @param {object} config to control the inner working
 * @return {object} promise that resolve all the files key query / mutation
 */
function processFiles(inDir, fileType, files, config) {
  // cache the function
  const fn = getResolver(inDir, fileType);
  // run
  return files.map(fn)
    .filter(obj => obj.ok)
    .map(obj => {
      const { type, name, file } = obj;
      const source = fs.readFileSync(file, 'utf8').toString();
      const result = astParser(source);
      const baseParams = result.body.filter(isExpression).map(extractParams).filter(p => p);
      const { description, params, returns } = extractExtraProps(config, result, source, name);
      // return
      return {
        [type]: {
          [name]: {
            file,
            description,
            params: merge([], baseParams[0], params), // merge array NOT OBJECT
            returns: returns // merge({}, takeDownArray(returns), returns)
          }
        }
      };
    })
    .reduce(merge, {
      query: {},
      mutation: {},
      auth: {},
      timestamp: Date.now()
    });
}

/**
 * get list of files and put them in query / mutation
 * @param {string} inDir diretory
 * @param {object} config pass config to the underlying method
 * @param {mixed} fileType we might do a ts in the future
 * @return {object} query / mutation
 */
function readFilesOutContract(inDir, config, fileType) {
  return new Promise((resolver, rejecter) => {
    glob(join(inDir, '**', ['*', fileType].join('.')), (err, files) => {
      if (err) {
        return rejecter(err);
      }
      resolver(
        processFiles(inDir, fileType, files, config)
      );
    });
  });
}

/**
 * Generate the final contract output to file
 * @param {object} config output directory
 * @param {object} contract processed result
 * @return {boolean} true on success
 */
function generateOutput(config, contract) {
  // this return a promise interface
  return new Promise((resolver, rejecter) => {
    const fileName = config.outputFilename || DEFAULT_FILE_NAME;
    const dist = join(config.outDir, fileName);
    if (fs.existsSync(dist)) {
      debug('Found existing contract [%s], removing', dist);
      fsx.removeSync(dist);
    }
    fsx.outputJson(dist, contract, {spaces: 2}, err => {
      if (err) {
        return rejecter(err);
      }
      resolver(dist);
    });
  });
}

// @BUG the async cause no end of problem for the client downstream
// so I take it down and only return promise instead
// main
module.exports = function(config) {
  debug('received config', config);
  console.log(`in ${config.inDir} out ${config.outDir} ${config.raw ? '[raw output]' : ''}${config.public ? '[public]': ''}`);
  const { inDir, outDir } = config;
  return readFilesOutContract(inDir, config, config.ext || EXT)
    .then(contract => {
      if (config.public) {
        return generateOutput(
          merge({}, config, { outputFilename: PUBLIC_FILE_NAME }),
          publicContractGenerator(config, contract)
        );
      }
      if (config.raw) {
        debug('raw output contract', contract);
        return contract;
      }
      return generateOutput(config, contract);
    });
}
