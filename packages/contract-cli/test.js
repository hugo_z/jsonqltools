#!/usr/bin/env node
const argv = require('yargs').argv;
const { explain, getJsdoc } = require('./lib/jsdoc');
const { join } = require('path');
const fs = require('fs-extra');
const debug = require('debug')('jsonql-contract:test-doc-api');
const { inspect } = require('util');
const merge = require('lodash.merge');

const file = join(__dirname, 'tests', 'fixtures', 'mutation', 'set-detail-obj.js');

const fn = fs.readFileSync(file, 'utf8').toString();
const out = getJsdoc(fn, 'setDetailObj');

debug('print the result', inspect(out, false, null));
