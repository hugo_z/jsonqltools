#!/usr/bin/env node
const argv = require('yargs').argv;
/**
 * Using Jsdoc-api to generate our contract file
 * https://www.npmjs.com/package/jsdoc-api
 */
const { generator, handler, remote } = require('./lib');
const { KEY_WORD } = require('jsonql-constants');
const { join } = require('path');
const debug = require('debug')('jsonql-contract:cli');
const fs = require('fs');
// create a message to tell the user where the file is
const checkFile = dist => {
  if (fs.existsSync(dist)) {
    return console.log('Your contract file generated in: %s', dist);
  }
  throw new Error('File is not generated!', dist);
};

// call it like this
// $ jsonql-contract /path/to/files /path/to/output
// or
// create a public-contract.json for client
// $ jsonql-contract /path/to/files /path/to/output --raw=1

remote(argv).then(result => {
  if (result === KEY_WORD) {
    return handler(argv)
      .then(generator)
      .then(checkFile)
      .catch(err => {
        console.log('json:ql contract-cli error!', err);
      });
  }
  checkFile(result);
});
